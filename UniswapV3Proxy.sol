// SPDX-License-Identifier: MIT
pragma solidity ^0.8.23;

import "@uniswap/v3-core/contracts/interfaces/IUniswapV3Pool.sol";
import "@uniswap/v3-core/contracts/interfaces/IUniswapV3Factory.sol";
import "@uniswap/v3-core/contracts/interfaces/IERC20Minimal.sol";

//import "@uniswap/v3-periphery/contracts/interfaces/INonfungiblePositionManager.sol";

contract UniswapV3Manager {
    event MintCallback(uint amount0Owned, uint amount1Owned);

    IUniswapV3Pool private uniswapV3Pool;

    function setPool(address _uniswapV3Pool) external {
        uniswapV3Pool = IUniswapV3Pool(_uniswapV3Pool);
    }

    function getPool() external view returns (IUniswapV3Pool) {
        return uniswapV3Pool;
    }

    function encodeAddress(
        address _payer
    ) external pure returns (bytes memory) {
        return abi.encode(_payer);
    }

    function mintPosition(
        int24 _lowerTick,
        int24 _upperTick,
        uint128 _liquidity,
        bytes calldata _data //адрес кошелька, закодированный через encodeAddress
    ) external {
        uniswapV3Pool.mint(
            msg.sender,
            _lowerTick,
            _upperTick,
            _liquidity,
            _data
        );
    }

    //коллбек, который вызывается после вызова ф-ии mintPosition у пула
    //колбек переводит на баланс пула _amount0Owned монет token0 и
    //_amount1Owned монет token1.
    //Перед этим нужно сделать approve() для каждого токена на адрес пула.
    function _mintCallback(
        uint _amount0Owned,
        uint _amount1Owned,
        bytes calldata _data
    ) external {
        address payer = abi.decode(_data, (address));
        emit MintCallback(_amount0Owned, _amount1Owned);

        if (_amount0Owned > 0) {
            IERC20Minimal(IUniswapV3Pool(msg.sender).token0()).transferFrom(
                payer,
                msg.sender,
                _amount0Owned
            );
        }

        if (_amount1Owned > 0) {
            IERC20Minimal(IUniswapV3Pool(msg.sender).token1()).transferFrom(
                payer,
                msg.sender,
                _amount1Owned
            );
        }
    }

    function initializePool(uint160 sqrtPriceX96) external {
        uniswapV3Pool.initialize(sqrtPriceX96);
    }
}

contract UniswapV3Proxy {
    //Mumbai Testnet addresses:
    //UniswapV3Factory 0x1F98431c8aD98523631AE4a59f267346ea31F984
    //WETH 0x734aeF51d427b2f745210Ec4BF1062ABd48Eceb6
    //WBTC 0x14e8733e7f178c77ed99faa08BBf042100Da4268
    //USDC 0x972aD3Fb93C9dFAE9B13e948AAfF0267f8C3e280
    //WMATIC 0x9c3C9283D3e44854697Cd22D3Faa240Cfb032889

    IUniswapV3Factory private immutable uniswapV3Factory;

    constructor(IUniswapV3Factory _uniswapV3Factory) {
        uniswapV3Factory = _uniswapV3Factory;
    }

    //создать пула
    function createPool(
        address _tokenA,
        address _tokenB,
        uint24 _fee
    ) external returns (address pool) {
        pool = uniswapV3Factory.createPool(_tokenA, _tokenB, _fee);
    }

    //получить адрес пула
    function getPoolAddress(
        address _tokenA,
        address _tokenB,
        uint24 _fee
    ) external view returns (address pool) {
        pool = uniswapV3Factory.getPool(_tokenA, _tokenB, _fee);
    }
}
