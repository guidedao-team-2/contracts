// SPDX-License-Identifier: GPL-2.0-or-later
pragma solidity ^0.8.20;

import "./external/IERC721Receiver.sol";
import "./external/IUniswapV3Pool.sol";
import "./external/IUniswapV3Factory.sol";
import "./external/TransferHelper.sol";
import "./external/INonfungiblePositionManager.sol";
import "./Pausable.sol";
import "./Ownable.sol";

/// @title A contract to manage UniswapV3 Positions
/// @author Liquidity-lark team
contract LiquidityExamples is IERC721Receiver, Pausable, Ownable {
    // INonfungiblePositionManager 0xC36442b4a4522E871399CD717aBDD847Ab11FE88
    // address public constant DAI = 0x6B175474E89094C44Da98b954EedeAC495271d0F;
    // goerli 0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6
    // mumbai 0x9c3C9283D3e44854697Cd22D3Faa240Cfb032889
    // address public constant USDC = 0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48;

    // FACTORY 0x1F98431c8aD98523631AE4a59f267346ea31F984
    //ucdc mainnet - 0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174
    //matic mainnen 0x0000000000000000000000000000000000001010

    // uint24 public constant poolFee = 100;

    //revert usdc 0xd871B40646E1a6dbDED6290B6B696459a69C68A0
    //revert matic 0x0000000000000000000000000000000000001010
    //https://ethereum.stackexchange.com/questions/138053/how-come-uniswap-0-3-percent-pool-fee-is-3000 значения fees

    // wmatic address 0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270 + usdc 0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174 + fee 500
    // -> pool 0xA374094527e1673A86dE625aa59517c5dE346d32, tickSpacing 10
    // tickUpper = -277800, tickLower = -278200

    // 0x3c499c542cEF5E3811e1192ce70d8cC03d5c3359 - другой usdc, не pos
    // 0xA374094527e1673A86dE625aa59517c5dE346d32 - пул верхней позиции
    // 0x7ceb23fd6bc0add59e62ac25578270cff1b9f619 - weth
    // 0,00005 weth -> 50000000000000 wei, 0.1 usds pos = 100000

    // 0.1 matic -> 100000000000000000 wei -> 100000000 gwei
    // https://brightinventions.pl/blog/single-swap-on-uniswap-v3-with-3-common-mistakes/ аппрув
    // Returned error: fee cap less than block base fee: address 0xb871997753283B5B202bB3394a0184302c58f91C, gasFeeCap: 421076463370 baseFee: 424585356409

    event PositionOpened(
        address indexed sender,
        uint256 indexed tokenId,
        OrderType indexed orderType
    );
    event PositionClosed(
        address indexed sender,
        uint256 indexed tokenId,
        OrderType indexed orderType
    );
    event FeesCollected(
        address indexed sender,
        uint256 indexed tokenId,
        OrderType indexed orderType
    );

    /// @notice Represents the deposit of an NFT
    struct Deposit {
        address owner;
        uint128 liquidity;
        address token0;
        address token1;
        uint24 poolFee;
        //uint256 token0Amount;
        //uint256 token1Amount;
        int24 tickLower;
        int24 tickUpper;
        OrderType orderType;
    }

    /// @notice Represents the type of an order
    enum OrderType {
        BUY,
        SELL
    }

    INonfungiblePositionManager public immutable nonfungiblePositionManager;
    IUniswapV3Factory public immutable factory;

    /// @dev deposits[tokenId] => Deposit
    mapping(uint256 => Deposit) public deposits;

    modifier onlyPositionOwner(uint256 tokenId) {
        require(msg.sender == deposits[tokenId].owner, "Not a position owner");
        _;
    }

    //Sepolia NonfungiblePositionManager 0x1238536071E1c677A632429e3655c799b22cDA52
    constructor(INonfungiblePositionManager _nonfungiblePositionManager) {
        nonfungiblePositionManager = _nonfungiblePositionManager;
        factory = IUniswapV3Factory(nonfungiblePositionManager.factory());
    }

    // Implementing `onERC721Received` so this contract can receive custody of erc721 tokens
    function onERC721Received(
        address operator,
        address,
        uint256 tokenId,
        bytes calldata
    ) external pure override returns (bytes4) {
        return this.onERC721Received.selector;
    }

    /// @notice Calls the mint function defined in periphery, mints the same amount of each token.
    /// For this example we are providing 1000 DAI and 1000 USDC in liquidity
    /// @return tokenId The id of the newly minted ERC721
    /// @return liquidity The amount of liquidity for the position
    /// @return amount0 The amount of token0
    /// @return amount1 The amount of token1
    function mintNewPosition(
        address token0,
        address token1,
        uint256 amount0ToMint,
        uint256 amount1ToMint,
        int24 tickLower,
        int24 tickUpper,
        uint24 poolFee,
        OrderType orderType
    )
        external
        onlyUnpaused
        returns (
            uint256 tokenId,
            uint128 liquidity,
            uint256 amount0,
            uint256 amount1
        )
    {
        // transfer tokens to contract
        TransferHelper.safeTransferFrom(
            token0,
            msg.sender,
            address(this),
            amount0ToMint
        );

        TransferHelper.safeTransferFrom(
            token1,
            msg.sender,
            address(this),
            amount1ToMint
        );

        // Approve the position manager
        TransferHelper.safeApprove(
            token0,
            address(nonfungiblePositionManager),
            amount0ToMint
        );
        TransferHelper.safeApprove(
            token1,
            address(nonfungiblePositionManager),
            amount1ToMint
        );

        INonfungiblePositionManager.MintParams
            memory params = INonfungiblePositionManager.MintParams({
                token0: token0,
                token1: token1,
                fee: poolFee,
                tickLower: tickLower,
                tickUpper: tickUpper,
                amount0Desired: amount0ToMint,
                amount1Desired: amount1ToMint,
                amount0Min: 0,
                amount1Min: 0,
                recipient: address(this),
                deadline: block.timestamp
            });

        // Note that the pool defined by DAI/USDC and fee tier 0.3% must already be created and initialized in order to mint
        (tokenId, liquidity, amount0, amount1) = nonfungiblePositionManager
            .mint(params);

        // Create a deposit
        _createDeposit(msg.sender, tokenId, orderType);

        // Remove allowance and refund in both assets.
        if (amount0 < amount0ToMint) {
            TransferHelper.safeApprove(
                token0,
                address(nonfungiblePositionManager),
                0
            );
            uint256 refund0 = amount0ToMint - amount0;
            TransferHelper.safeTransfer(token0, msg.sender, refund0);
        }

        if (amount1 < amount1ToMint) {
            TransferHelper.safeApprove(
                token1,
                address(nonfungiblePositionManager),
                0
            );
            uint256 refund1 = amount1ToMint - amount1;
            TransferHelper.safeTransfer(token1, msg.sender, refund1);
        }

        emit PositionOpened(msg.sender, tokenId, orderType);
    }

    ///
    /// @param tokenId The id of the position
    /// @return nonce
    /// @return operator
    /// @return token0
    /// @return token1
    /// @return fee
    /// @return tickLower
    /// @return tickUpper
    /// @return liquidity
    /// @return feeGrowthInside0LastX128
    /// @return feeGrowthInside1LastX128
    /// @return tokensOwned0
    /// @return tokensOwned1
    function getPositionInfo(
        uint tokenId
    )
        external
        view
        returns (
            uint96 nonce,
            address operator,
            address token0,
            address token1,
            uint24 fee,
            int24 tickLower,
            int24 tickUpper,
            uint128 liquidity,
            uint feeGrowthInside0LastX128,
            uint feeGrowthInside1LastX128,
            uint128 tokensOwned0,
            uint128 tokensOwned1
        )
    {
        return nonfungiblePositionManager.positions(tokenId);
    }

    function closePosition(uint256 tokenId) external {
        //1) check if msg.sender is owner or we out of range(BUY/SELL order)
        require(
            msg.sender == deposits[tokenId].owner || canClosePosition(tokenId)
        );
        //2) _decreaseLiquidity
        _decreaseLiquidity(tokenId);
        //3) collectAllFees
        collectAllFees(tokenId);

        //burn the nft
        nonfungiblePositionManager.burn(tokenId);
        delete deposits[tokenId];

        emit PositionClosed(msg.sender, tokenId, deposits[tokenId].orderType);
    }

    function pause() external onlyOwner {
        _pause();
    }

    function unpause() external onlyOwner {
        _unpause();
    }

    /// @notice Collects the fees associated with provided liquidity
    /// @dev The contract must hold the erc721 token before it can collect fees
    /// @param tokenId The id of the erc721 token
    /// @return amount0 The amount of fees collected in token0
    /// @return amount1 The amount of fees collected in token1
    function collectAllFees(
        uint256 tokenId
    )
        public
        returns (uint256 amount0, uint256 amount1)
    {
        // Caller must own the ERC721 position, meaning it must be a deposit

        // set amount0Max and amount1Max to uint256.max to collect all fees
        // alternatively can set recipient to msg.sender and avoid another transaction in `sendToOwner`
        INonfungiblePositionManager.CollectParams
            memory params = INonfungiblePositionManager.CollectParams({
                tokenId: tokenId,
                recipient: address(this),
                amount0Max: type(uint128).max,
                amount1Max: type(uint128).max
            });

        (amount0, amount1) = nonfungiblePositionManager.collect(params);

        // send collected feed back to owner
        _sendToOwner(tokenId, amount0, amount1);
        emit FeesCollected(msg.sender, tokenId, deposits[tokenId].orderType);
    }

    function canClosePosition(uint256 tokenId) public view returns (bool) {
        Deposit memory position = deposits[tokenId];

        (int24 currTick, ) = getTickInfo(
            position.token0,
            position.token1,
            position.poolFee
        );

        return
            (position.orderType == OrderType.BUY &&
                currTick > position.tickUpper) ||
            (position.orderType == OrderType.SELL &&
                currTick < position.tickLower);
    }

    function _createDeposit(
        address owner,
        uint256 tokenId,
        OrderType orderType
    ) internal {
        (
            ,
            ,
            address token0,
            address token1,
            uint24 poolFee,
            int24 tickLower,
            int24 tickUpper,
            uint128 liquidity,
            ,
            ,
            ,

        ) = nonfungiblePositionManager.positions(tokenId);

        // set the owner and data for position
        // operator is msg.sender
        deposits[tokenId] = Deposit({
            owner: owner,
            liquidity: liquidity,
            token0: token0,
            token1: token1,
            poolFee: poolFee,
            tickLower: tickLower,
            tickUpper: tickUpper,
            orderType: orderType
        });
    }

    function _decreaseLiquidity(
        uint256 tokenId
    ) internal returns (uint256 amount0, uint256 amount1) {
        // get liquidity data for tokenId
        uint128 liquidity = deposits[tokenId].liquidity;

        // amount0Min and amount1Min are price slippage checks
        // if the amount received after burning is not greater than these minimums, transaction will fail
        INonfungiblePositionManager.DecreaseLiquidityParams
            memory params = INonfungiblePositionManager
                .DecreaseLiquidityParams({
                    tokenId: tokenId,
                    liquidity: liquidity,
                    amount0Min: 0,
                    amount1Min: 0,
                    deadline: block.timestamp
                });

        (amount0, amount1) = nonfungiblePositionManager.decreaseLiquidity(
            params
        );

        //send liquidity back to owner
        //_sendToOwner(tokenId, amount0, amount1);
    }

    /// @notice Transfers funds to owner of NFT
    /// @param tokenId The id of the erc721
    /// @param amount0 The amount of token0
    /// @param amount1 The amount of token1
    function _sendToOwner(
        uint256 tokenId,
        uint256 amount0,
        uint256 amount1
    ) internal {
        // get owner of contract
        address owner = deposits[tokenId].owner;

        address token0 = deposits[tokenId].token0;
        address token1 = deposits[tokenId].token1;
        // send collected fees to owner
        TransferHelper.safeTransfer(token0, owner, amount0);
        TransferHelper.safeTransfer(token1, owner, amount1);
    }

    function getTickInfo(
        address token0,
        address token1,
        uint24 poolFee
    ) public view returns (int24 tick, int24 tickSpacing) {
        IUniswapV3Pool pool = IUniswapV3Pool(
            factory.getPool(token0, token1, poolFee)
        );
        require(address(pool) != address(0), "The pool doesn't exist");
        (, tick, , , , , ) = pool.slot0();
        tickSpacing = pool.tickSpacing();
    }
}
