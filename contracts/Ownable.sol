// SPDX-License-Identifier: SEE LICENSE IN LICENSE
pragma solidity >=0.7.6;

abstract contract Ownable {
    address internal owner;

    constructor() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Not an owner");
        _;
    }

    function getOwner() external view returns (address) {
        return owner;
    }
}
