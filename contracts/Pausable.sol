// SPDX-License-Identifier: SEE LICENSE IN LICENSE
pragma solidity >=0.7.6;

abstract contract Pausable {
    event Paused(address owner);
    event Unpaused(address owner);

    bool internal paused;

    modifier onlyPaused() {
        require(paused, "Unpaused");
        _;
    }

    modifier onlyUnpaused() {
        require(!paused, "Paused");
        _;
    }

    function _pause() internal virtual {
        paused = true;
        emit Paused(msg.sender);
    }

    function _unpause() internal virtual {
        paused = false;
        emit Unpaused(msg.sender);
    }
}
