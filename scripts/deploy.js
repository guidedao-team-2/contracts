// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const hre = require("hardhat");

async function main() {
  const nonfungiblePositionManager =
    "0x1238536071E1c677A632429e3655c799b22cDA52";

  const liquidityExamples = await hre.ethers.deployContract(
    "LiquidityExamples",
    [nonfungiblePositionManager]
  );

  await liquidityExamples.waitForDeployment();

  console.log(`liquidityExamples deployed to ${liquidityExamples.target}`);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
